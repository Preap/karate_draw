<?php

session_start();
include 'connect.php';
include 'bootstrap.php';


$scoresheetID = $_SESSION["scoresheetID"];

//find number of athletes
$sql = "SELECT COUNT(DISTINCT athleteID) 
        FROM participants
        WHERE scoresheetID = '$scoresheetID';";

$result = $con->query($sql);

if ($result->num_rows > 0) 
{
    // output data of each row
    while($row = $result->fetch_assoc()) 
    {
       $numberOfAthletes = $row["COUNT(DISTINCT athleteID)"];
    }
  
}

$_SESSION["numberOfAthletes"]=$numberOfAthletes;

//find number of teams
$sql = "SELECT COUNT(DISTINCT clubName) 
        FROM participants
        INNER JOIN athlete
        ON participants.athleteID = athlete.athleteID
        WHERE scoresheetID = '$scoresheetID';";

$result = $con->query($sql);

if ($result->num_rows > 0) 
{
    // output data of each row
    while($row = $result->fetch_assoc()) 
    {
       $numberOfTeams = $row["COUNT(DISTINCT clubName)"];
    }
  
}




//Create a stack for each team
for($i=0;$i < $numberOfTeams;$i++)
{
    $stack{$i} = new SplStack;
    
    $sql="SELECT DISTINCT clubName
            FROM participants 
            INNER JOIN athlete 
            ON participants.athleteID = athlete.athleteID 
            WHERE scoresheetID = '$scoresheetID'
            ORDER BY clubName ASC;";
    
    $result = $con->query($sql);

    if ($result->num_rows > 0) 
    {
        $num=0;
        while($row = $result->fetch_assoc())  //find teamName
        {
            
            
            $teamName = $row["clubName"];
            $team{$num} = $teamName;
            $num++;
        }
    }
    
    
}

//fill in the stacks of each team with the coresponding players
for($i=0;$i < $numberOfTeams; $i++)
{
    $sql="SELECT athlete.athleteID 
        FROM participants 
        INNER JOIN athlete 
        ON participants.athleteID = athlete.athleteID 
        WHERE scoresheetID = '$scoresheetID' and athlete.clubName= '$team[$i]';";

    $result = $con->query($sql);

    if ($result->num_rows > 0) 
    {

        while($row = $result->fetch_assoc())  //find teamName
        {
            $athleteID = $row["athleteID"];
            $stack[$i]->push($athleteID);

    //        echo"Athlete : ".$athleteID." pushed in stack[".$i."] and is part of ".$team[$i]."<br>";
        }
    }
    randomizeTheStack($stack[$i]);
}//end of for


//randomize the positions of players of each team in their stack
//making the draw more random
function randomizeTheStack($stack)
{
    for($i = 0;$i < $stack->count();$i++)
    {
    $firstID = rand(0, $stack->count() - 1);
    $temp = $stack[$firstID];
    $secondID = rand(0,$stack->count()-1);
    $stack[$firstID] = $stack[$secondID];
    $stack[$secondID] = $temp;
    }
}
?>