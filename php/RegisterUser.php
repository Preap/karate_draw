<?php
session_start();
if (empty($_SESSION['username']) OR ($_SESSION['admin']!=1))
{
	header('Location: index.php');
}
include 'email_sender.php';
session_start();
$errors = array();
include "connect.php";
	if(empty($_POST['clubname']))
	{
		$errors[]='You forgot to enter your Club Name.';
	}
	else
	{
		$clubname=mysqli_escape_string($con,$_POST['clubname']);
	}

	$e1 = $_POST["email1"];
	if (empty($_POST['email1']))
	{
		$errors[] = 'You forgot to enter your email';
	}
	else
	{
		if (!filter_var($e1, FILTER_VALIDATE_EMAIL))
		{
  			$errors[] = 'Invalid email format';
		}
		else
		{
			$check_email=mysqli_query($con, "SELECT * FROM account WHERE email like '$e1'");
			if (!$check_email)
			{
				$errors[] = 'This email already exists';
			}
			else
			{
				$email=mysqli_escape_string($con,$e1);
			}
		}
	}

	if(preg_match("/^[0-9]{8}$/", $_POST['phone']) or $_POST['phone']==null)
	{
		$phone=intval(trim($_POST['phone']));
	}
	else
	{
		$errors[]='Your phone number is not valid.';
	}
	
	$address=mysqli_escape_string($con,$_POST['address']);
	$ref=mysqli_escape_string($con,$_POST['ref']);
	if(empty($_POST['username']))
	{
		$errors[]='You forgot to enter your username.';
	}
	else
	{
		$u=$_POST['username'];
		$check_username=mysqli_query($con, "SELECT * FROM account WHERE username like '$u'");
		if (!$check_username)
		{
			$errors[] = 'This username already exists';
		}
		else
		{
			$username=mysqli_escape_string($con,$u);
		}

	}
	if(!empty($_POST['password1']))
	{
		$p1=$_POST['password1'];
		$p2=$_POST['password2'];
		if ($p1!=$p2)
		{
			$errors[]='Your password did not match the confirmed password.';
		}
		else
		{
			$password=trim($p1);
		}
	}
	else
	{
		$errors[]='You forgot to enter your password.';
	}
	$admin=0;
	if (empty($errors))
	{
		$q="INSERT INTO account(Username,Password,ClubName,Email,Address,Phone,City,admin) VALUES ('$username','$password','$clubname','$email','$address','$phone','$city','$admin')";
		$r=mysqli_query($con, $q);
		if ($r)
		{
			//echo "<script type='text/javascript'>alert('submitted successfully!')</script>";
			$to= $email; // Send email to our user
			$subject = "Fudokan KARATE Cyprus"; // Give the email a subject
			$message = "

 	Dear '$clubname',
	The Administrator of Fudokan Karate Cyprus has created an account to this link:
	http://localhost/karate/php/index.php
	Username:'$username'
	Password:'$password'
	ENJOY,

	FUDOKAN KARATE CYPRUS";
			send_email($to, $subject, $message);
		}
		else
		{
			echo '<p>System error</p>';
			echo mysqli_error($con);
		}
		mysqli_close($con);
		header("Location: AdminStartPage.php?value=success");
	}
	else
	{?>
		<html> <h1>ERROR!</h1>
		 <p class="error"> The following error(s) occured:<br /> </p></html>
		<?php
		foreach ($errors as $msg){?>
			 <html> <p>- <?php echo $msg;?><br /> </p> <?php }?>
			<p>Please try again!</p><p><br /></p> </html>

<html>
	<head>
		<title>Register!</title>
		<link href="style.css" type="text/css" rel="stylesheet" />
	</head>
	<body>
		<fieldset>
                    <link href="style.css" type="text/css" rel="stylesheet" />
		<h1><img src="logo.jpg" alt="Logo" style="width:150px" align="center" >Register a Club Account:</h1>
		 <form  action="RegisterUser.php" method="post"  >
		 	E-mail:*<br>
			<input type="text" name="email1" required="required" maxlength="50" value="<?php if(isset($_POST['email'])){echo $_POST["email"];}?>"><br>
			Username:*<br>
			<input type="text" name="username" required="required" maxlength="20" value="<?php if(isset($_POST['username'])){echo $_POST["username"];}?>"><br>
			Password:*<br>
			<input type="password" name="password1" required="required" maxlength="20"><br>
			Confirm password:*<br>
			<input type="password" name="password2" required="required" maxlength="20"><br>
			Club Name:*<br>
			<input type="text" name="clubname" required="required" maxlength="20" value="<?php if(isset($_POST['clubname'])){echo $_POST["clubname"];}?>"><br>
			Address:<br>
			<input type="text" name="address" maxlength="80" value="<?php if(isset($_POST['address'])){echo $_POST["address"];}?>"><br>
			City:<br>
			<input type="text" name="city" maxlength="20" value="<?php if(isset($_POST['city'])){echo $_POST["city"];}?>"><br>
			Phone:<br>
			<input type="number" name="phone" maxlength="12" value="<?php if(isset($_POST['phone'])){echo $_POST["phone"];}?>"><br>
			<p>*Required</p>
			<input type="submit" value="Submit">
		 </form>
		</fieldset>
	</body>
</html>
<?php unset($errors);
}?>
