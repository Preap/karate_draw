<!DOCTYPE html>
	<html>
		<head>
			<title>Create new password!</title>
                        <link href="style.css"  rel="stylesheet" type="text/css"/>
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">	
			<?php
				$e=$_GET['error'];
				if ($_GET['error']==2)
				{
					echo "<script type='text/javascript'>alert('Your password did not match the confirmed password. Please try again!')</script>";
				}
				$resetkey=$_GET['id'];
				
				if ($resetkey==0 || $resetkey==null || $resetkey=="")
				{
					header ('Location: index.php?error=2');
				}
			?>		
		</head>
		<body>
    		<h1><img src="logo.jpg" alt="Logo" style="width:300px" align="center" >Reset Password:</h1>
			 <form  action="newpassword.php?id=<?php echo "$resetkey";?>" method="post" >
  				New password:<br>
  				<input type="password" name="password" required="required"><br>
  				Confirm new password:<br>
  				<input type="password" name="cpassword" required="required"><br>
  				<input type="submit" value="Submit">
			 </form>
		</body>
	</html>
