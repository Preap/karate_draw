<!DOCTYPE html>

<html>
    <head>
        <?php
        include 'bootstrap.php';
        ?>
        
         <!-- Latest compiled and minified CSS -->
        <meta charset="UTF-8">
        <title>Reports Management</title>
    </head>
    <body>
        <h1 class="text-center">REPORTS MANAGEMENT</h1>
        
        <div class="container" col-sm-8>
            
            <a href="TournamentReport.php">
            <button type="button" class="btn btn-primary btn-lg btn-block">Tournament Report</button>
            </a> <br>
            
            <a href="ClubReport.php">
            <button type="button" class="btn btn-primary btn-lg btn-block">Club Report</button>
            </a> <br>
            
            <a href="AthleteReport.php">
            <button type="button" class="btn btn-primary btn-lg btn-block">Athlete Report</button>
            </a>
            
        </div>
    </body>
</html>