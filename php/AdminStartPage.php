<?php
session_start();

if (empty($_SESSION['username']) or ($_SESSION['admin'] != 1)) {
    header('Location: index.php');
} ?>
<!DOCTYPE html>

<html>
    <head>
        <style>
            <?php include 'style.css'; ?>
        </style>
        <?php
        $value = !empty($_GET['value']);
        if ($value) {
            echo "<script type='text/javascript'>alert('Succesful submition!')</script>";
        }
        include 'bootstrap.php';

        ?>
        
         <!-- Latest compiled and minified CSS -->
        <meta charset="UTF-8">
        <title>Admin Page</title>
        <style>
            .cardlink{
                color: black;
            }  
        </style>
    </head>
    <body>
        <h1 font-weight='bold' align='center' ><img src="logo.jpg" alt="Logo" style="width:200px" align="center" >Fudokan Karate Cyprus</h1>
         </ br>
         <h1 font-weight='bold' align='center'>&nbsp;&nbsp;&nbsp;Administration Page</h1>
         </ br>
        <form action='../php/tournament/index.php' request='get'>
            <input type="submit" value="Tournaments Management">
        </form>         
        <form action='../php/athlete/index.php' request='get'>
            <input type="submit" value="Athlete Management">
        </form>
         <form action='../php/showActiveTournaments.php' request='get'>
            <input type="submit" value="Execute Draw">
        </form>
         <form action='../php/tcpdf/index.php' request='get'>
        	<input type="submit" value="Report Management">
        </form>
        <form action='RegisterUser.html' request='get'>
        	<input type="submit" value="Register A Club-Account">
        </form>
        <form action='RegisterAdmin.html' request='get'>
        	<input type="submit" value="Register An Admin">
        </form>
        <form action='logout.php' request='post'>
        	<input type="submit" value="Logout">
        </form>
                   
    </body>
</html>

<script>
function goto(destination)
{
    if(destination == 'TournamentManagement' )
    {
        window.location = "index.php";
    }
}
</script>