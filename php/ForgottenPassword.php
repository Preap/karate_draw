<!DOCTYPE html>

<html>
    <head>
        <?php
        include 'bootstrap.php';
        ?>
        
         <!-- Latest compiled and minified CSS -->
        <meta charset="UTF-8">
        <title>Forgotten Password</title>
        <link href="style.css"  rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="container col-sm-8">
          <h2 class="text-center" >Don't Worry!</h2>
          <p class="text-center" >Just enter your email in order to reset your password</p>
          <form action="ResetPassword.php" method="post">
            <div class="form-group">
              <label for="email">Email:</label>
              <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
            </div>
            <br><br>
            <input type="submit" value="Reset Password"</input>
          </form>
        </div>
    </body>
</html>
