<?php
session_start();
include '../connect.php';
include '../bootstrap.php';
$username = $_SESSION["username"];
$clubName = getClubName($username);
$tournamentID = $_POST["T_ID"];
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Chose athletes to register for this tournament</title>
        <link href="../style.css"  rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="./dist//jquery.bracket.min.css" rel="stylesheet">
    </head>
    <body>
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.11.3.js"></script>
        <script src="./dist/jquery.bracket.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script> 
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <h1 align="center"> Chose athletes to register for this tournament </h1>
        <div align="center">
            <div id="tournamentID" hidden="true"><?php echo $tournamentID ?></div>
            <table id ="theTable" class="table table-striped table-hover" style="width: 900px">
                <tr>
                    <th hidden>ID</th>
                    <th>Name</th>
                    <th>Surname</th>
                    <th>Date of Birth</th>
                    <th>Gender</th>
                    <th>Kumite</th>
                    <th>Kata</th>
                    <th>Kihon</th>
                    <th>Fukuoka</th>
                </tr>
                <?php 
                $sql = "SELECT * "
                    . "FROM athlete "
                    . "WHERE athlete.clubName = '$clubName' ;";
                $result = mysqli_query($con, $sql);
                if (mysqli_num_rows($result) > 0) {
                    while ($row = $result->fetch_assoc()) {
                        echo "<tr id=" . $row["athleteID"] . "><td hidden>" . $row["athleteID"] . "</td>"
                            . "<td>" . $row["firstName"] . "</td>"
                            . "<td>" . $row["lastName"] . "</td>"
                            . "<td>" . $row["dob"] . "</td>"
                            . "<td>" . $row["gender"] . "</td>"
                            . "<td><input type=\"checkbox\" name=" . $row["athleteID"] . " value =\"kumite\"></td>"
                            . "<td class=\"cb\"><input type=\"checkbox\" name=" . $row["athleteID"] . " value =\"kata\"></td>"
                            . "<td><input type=\"checkbox\" name=" . $row["athleteID"] . " value =\"kihon\"></td>"
                            . "<td><input type=\"checkbox\" name=" . $row["athleteID"] . " value =\"Fukuoka\"></td>"
                            . "</tr>";
                    }
                }
                ?>
            </table>
        </div>
        <div style="position: relative;" align="center">
                <button id="registerButton" name="singlebutton" class="btn btn-lg btn-danger center-block">REGISTER FOR THE TOURNAMENT</button>
        </div>
    </body>
    <script src="./selectAthletesToRegister.js"></script> 
</html>  

<?php

function getClubName($un) : string
{
    include '../connect.php';
    $clubName = "";
    $sql = "SELECT * 
            FROM account
            WHERE Username = '$un';";
    $result = mysqli_query($con, $sql);

    if (mysqli_num_rows($result) > 0) {
        while ($row = $result->fetch_assoc()) {
            $clubName = $row["ClubName"];
            return $clubName;
        }
    }
}
?>