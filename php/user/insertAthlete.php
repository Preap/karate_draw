<?php
session_start();
include '../connect.php';
include '../bootstrap.php';
$username = $_SESSION["username"];

$name = $_POST["name"];
$surname = $_POST["surname"];
$email = $_POST["email"];
$gender = $_POST["gender"];
$clubName = getClubName($username);
$school = $_POST["school"];
$phone = $_POST["phone"];
$belt = $_POST["belt"];
$date = $_POST["date"];
$id = $_POST["id"];

$sql = "INSERT INTO `athlete` (`id`,`firstName`, `lastName`,"
    . " `email`, `dob`, `gender`, `clubName`, `school`,"
    . " `phone`, `belt`)"
    . " VALUES ('$id', '$name', '$surname', '$email', '$date', '$gender', '$clubName', '$school','$phone', '$belt'); ";

if ($con->query($sql) === true) {
    insertOK($name, $surname);
} else {
    insertBad();
}


function insertOK($n, $s)
{
    $htmlString = '
<!DOCTYPE html>
<html>
    <head>
        <title>Welcome!</title>
        <link href="../style.css"  rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <h1 align="center">
            ' . $n . $s . '   registered succesfuly!
        </h1>
        <div style="position: relative; top:20%" align="center">
            <a href="./addNewAthlete.php">
                <button  id="singlebutton" name="singlebutton" class="btn btn-lg btn-danger center-block">Register Another Athlete</button>
            </a>
            <a href="../userhome.php">
                <button  id="singlebutton" name="singlebutton" class="btn btn-lg btn-danger center-block">Done!</button>
            </a>
        </div>
    </body>
</html>';
    echo $htmlString;
}

function insertBad()
{
    $htmlString = '
<!DOCTYPE html>
<html>
    <head>
        <title>Welcome!</title>
        <link href="../style.css"  rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <h1 align="center">
            You have already registered athlete with this ID in your team!
        </h1>
        <div style="position: relative; top:20%" align="center">
            <a href="./addNewAthlete.php">
                <button  id="singlebutton" name="singlebutton" class="btn btn-lg btn-danger center-block">Register Another Athlete</button>
            </a>
            <a href="../userhome.php">
                <button  id="singlebutton" name="singlebutton" class="btn btn-lg btn-danger center-block">Done!</button>
            </a>
        </div>
    </body>
</html>';
    echo $htmlString;
}

function getClubName($un) : string
{
    include '../connect.php';
    $clubName = "";
    $sql = "SELECT *
            FROM account
            WHERE Username = '$un';";
    $result = mysqli_query($con, $sql);

    if (mysqli_num_rows($result) > 0) {
        while ($row = $result->fetch_assoc()) {
            $clubName = $row["ClubName"];
            return $clubName;
        }
    }
}
?>

        
        
