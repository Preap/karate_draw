<?php
session_start();
include '../connect.php';
include '../bootstrap.php';
$username = $_SESSION["username"];
$clubName = getClubName($username);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Welcome!</title>
        <link href="../style.css"  rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <h1 align="center">
            REGISTER NEW ATHLETE IN YOUR CLUB
        </h1>
    </body>
    <form action="./insertAthlete.php" method="post">
        <label><b>Name*</b></label>
        <input type="text" placeholder="Enter name" name="name" required><br>
            
        <label><b>Surname*</b></label>
        <input type="text" placeholder="Enter surname" name="surname" required><br> 
            
        <label><b>Gender*</b></label><br>
        <input type="radio" name="gender" value="MALE" required> Male<br>
        <input type="radio" name="gender" value="FEMALE" required> Female<br>
        
        <label><b>Date of Birth*</b></label><br>
        <input type="date" name="date" required=><br>
            
        <label><b>ID Number*</b></label>
        <input type="text" placeholder="Enter id" name="id" required><br>
            
        <label for="email"><b>Email</b></label>
        <input type="text" placeholder="Enter email" name="email"><br>
            
        <label><b>School</b></label>
        <input type="text" placeholder="School Name" name="school"><br>
            
        <label><b>Phone</b></label>
        <input type="text" placeholder="Enter phone" name="phone"><br>
            
        <div align="center">
            <label><b>Belt</b></label>
            <select name="belt" id="belt" class="form-control" style="width: 300px;">
                <option value="Yellow"> Yellow</option>
                <option value="Orange">Orange</option>
                <option value="Green">Green</option>
                <option value="Blue">Blue</option>
                <option value="Purple">Purple</option>
                <option value="Brown">Brown</option>
                <option value="Red">Red</option>
                <option value="Black">Black</option>
            </select>
        </div>
        <br/>
            
        <div style="position: relative; top:50%" align="center">
            <button  type="submit" id="registerButton" name="singlebutton" class="btn btn-lg btn-danger center-block">REGISTER!</button>
        </div>
    </form>
</html>
    
<?php
function getClubName($un) : string
{
    include '../connect.php';
    $clubName = "";
    $sql = "SELECT *
            FROM account
            WHERE Username = '$un';";
    $result = mysqli_query($con, $sql);

    if (mysqli_num_rows($result) > 0) {
        while ($row = $result->fetch_assoc()) {
            $clubName = $row["ClubName"];
            return $clubName;
        }
    }
}
?>