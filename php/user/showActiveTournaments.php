<?php
session_start();
include '../connect.php';
include '../bootstrap.php';
?>


<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $sql = "SELECT * FROM tournament;";

        $result = mysqli_query($con, $sql);

        ?>
        <form action="./selectAthletesToRegister.php" method="post">
            
            <?php
            $activeTournaments = "<table class=\"table table-striped table-hover\">
                                    <tr>
                                    <th class=\"col-sm-1\">Tournament ID</th>
                                    <th>Tournament Name</th>
                                    <th>Tournament Venue</th>
                                    </tr>";


            echo $activeTournaments;


            if (mysqli_num_rows($result) > 0) {
                while ($row = $result->fetch_assoc()) {
                    echo "<tr><td>" . $row["id"] . "</td>"
                        . "<td>" . $row["tournamentName"] . "</td>"
                        . "<td>" . $row["venue"] . "</td>"
                        . "</tr>";
                }
                echo "</table>";
            }
            ?>
        
            <input type="submit" value="test" name="test" />
            <input type="text" value="value111" name="T_ID" id="text" hidden="true">
        </form>
        
        <script>
        
//        Select a tournament from the table and get its Tournament_ID
        $("tr").click(function(){   
        $(this).addClass("bg-primary");
        $(this).siblings().removeClass("bg-primary");
        document.getElementById("text").value=$(this).children("td:first-of-type").text();
         
        }); 
        </script>
            
    </body>
</html>