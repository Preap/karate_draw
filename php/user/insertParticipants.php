<?php
include '../connect.php';

$registrations = $_POST["registrations"];

$tournamentID = $_POST["tid"];

//echo ("The tournamentID is: ". json_encode($tournamentID));

// echo (print_r($registrations[0])); 

for ($i = 0; $i < sizeof($registrations); $i++) {
    $id = json_encode($registrations[$i]['id']);
    $id = trim($id, "\"");
    $id = intval($id);  //trim and cast id to integer number
    $discipline = json_encode($registrations[$i]['discipline']);
    $discipline = trim($discipline,"\"");
    $ageCategory = getAthleteAgeCategory($id);
    $gender = getAthleteGender($id);
    
    $sql = "SELECT `scoresheetID` "
            . "FROM `scoresheet` "
            . "WHERE `tournamentID` = 1 "
            . "and `gender` = '$gender' "
            . "and `ageCategory` = '$ageCategory' "
            . "and `discipline` = '$discipline';";
    
    $result = mysqli_query($con, $sql);
    if (mysqli_num_rows($result) > 0)
    {
        while($row = $result->fetch_assoc()) 
        {
           $scoresheetID = $row["scoresheetID"];
        }
    }
    else
    {
     echo "No scoresheetID found";
    }
    
    $sql = "INSERT INTO `participants` (`scoresheetID`, `athleteID`) 
        VALUES ('$scoresheetID', '$id');";

    if ($con->query($sql) === TRUE) {
        //echo "New record created successfully";
    } else {
        echo "Error: " . $sql . "<br>" . $con->error;
    }
}






/**
 * This function returns the gender of the given Athlete
 */
function getAthleteGender($athleteID)
{
    include '../connect.php';
    $sql = "SELECT `gender` FROM `athlete` WHERE `athleteID` = '$athleteID'";
//    echo $sql;
    $result = mysqli_query($con, $sql);
    if (mysqli_num_rows($result) > 0)
    {
        while($row = $result->fetch_assoc()) 
        {
           $gender = $row["gender"];
           return $gender;
        }
    }
    else
    {
     echo "No athlete with ID: $athleteID found";
     return null;
    }
}

/*
 * This function returns the age category of the athlete
 */
function getAthleteAgeCategory($athleteID){
    include '../connect.php';
    $sql = "SELECT `dob` FROM `athlete` WHERE `athleteID` = '$athleteID'";
    
    $result = mysqli_query($con, $sql);
    if (mysqli_num_rows($result) > 0)
    {
        while($row = $result->fetch_assoc()) 
        {
           $dob = $row["dob"];
        }
    }
    else
    {
     echo "No athlete with ID: $athleteID found";
     return null;
    }
    
    $tz  = new DateTimeZone('Europe/Brussels');
    $age = DateTime::createFromFormat('Y-m-d', $dob, $tz)
     ->diff(new DateTime('now', $tz))
     ->y;
    
    if($age < 14){
        return "Children";
    }else if($age < 16){
        return "Caddets";
    }else if($age < 18){
        return "Junior";
    }else if($age < 21){
        return "Youth";
    }else{
        return "Senior";
    }
}
