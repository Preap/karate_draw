<?php
session_start();
include '../connect.php';
include '../bootstrap.php';
$username = $_SESSION["username"];
$clubName = getClubName($username);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Welcome!</title>
        <link href="../style.css"  rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <h1 align="center"> Hello <?php echo $clubName; ?></h1>
        <div align="center">
            <table class="table table-striped table-hover" style="width: 700px;">
                <tr>
                    <th style="width: 175px">Name</th>
                    <th style="width: 175px">Surname</th>
                    <th style="width: 175px">Date of Birth</th>
                    <th style="width: 175px">Gender</th>
                </tr>
            <?php 
            $sql = "SELECT * "
                . "FROM athlete "
                . "WHERE athlete.clubName = '$clubName' ;";
            $result = mysqli_query($con, $sql);
            if (mysqli_num_rows($result) > 0) {
                while ($row = $result->fetch_assoc()) {
                    echo "<tr><td>" . $row["firstName"] . "</td>"
                        . "<td>" . $row["lastName"] . "</td>"
                        . "<td>" . $row["dob"] . "</td>"
                        . "<td>" . $row["gender"] . "</td>"
                        . "</tr>";
                }
                echo "</table>";
            }
            ?>
            </table>
        </div>
        <div style="position: relative;" align="center">
            <a href="./addNewAthlete.php">
                <button  id="singlebutton" name="singlebutton" class="btn btn-lg btn-danger center-block">ADD NEW ATHLETE</button>
            </a>
        </div>
    </body>
</html>
    
<?php 


function getClubName($un) : string
{
    include '../connect.php';
    $clubName = "";
    $sql = "SELECT *
            FROM account
            WHERE Username = '$un';";
    $result = mysqli_query($con, $sql);

    if (mysqli_num_rows($result) > 0) {
        while ($row = $result->fetch_assoc()) {
            $clubName = $row["ClubName"];
            return $clubName;
        }
    }
}

function getAge($date) : int
{

}