$('#registerButton').click(function() {
  tid = $('#tournamentID').text();
//  console.log(tid);
  checkBoxes = $('input:checked').toArray(); //put all checked checkboxes to array
  var registrationArray = []; //an array to store all registration combinations of id and discipline

  for (var i = 0; i < checkBoxes.length; i++) {
    var registration = [];
    var id = checkBoxes[i].name;
    var cb = checkBoxes[i].value;

    registration = { id: id, discipline: cb };

    registrationArray.push(registration);
  }

  $.post(
    './insertParticipants.php',
    { registrations: registrationArray,
      tid: tid},
    function(data) {
      console.log(data);
    }
  );
});
