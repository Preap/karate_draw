<?php
session_start();
include './connect.php';
include './bootstrap.php';
?>
    
    
<!DOCTYPE html>
    
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $tournamentID = $_POST["T_ID"];
        
        $sql = "SELECT `age_category`,`disciplines` "
         . "FROM `tournament` WHERE `id`='$tournamentID';";
 
        $result = mysqli_query($con, $sql);

        if (mysqli_num_rows($result) > 0)
        {
           $row = $result->fetch_assoc();
           $ageCategories = $row["age_category"];
           $disciplines = $row["disciplines"];
           $categoriesArray = explode(",", $ageCategories);
           $disciplinesArray = explode(",",$disciplines);
        }
            
        ?>
        <div hidden="true" id="tournamentID"><?php echo $tournamentID?></div>
        <div class="container" style="margin-top: 15%; margin-left: 20%; font-size: 24;">
            <form action="./showRegisteredAthletes.php" method="post">
                <input hidden="true"  value="" id="scoresheetID" name="scoresheetID" type="text">
                <div class="row">
                    <div class="col-25">
                        <label for="country">Age Category</label>
                    </div>
                    <div class="col-75" style="margin-left: 20">
                        <select id="ageCategory" name="ageCategory">
                            <?php
                                    foreach ($categoriesArray as $category){
                                        echo "<option value=\"$category\">$category</option>";
                                    }                            
                            ?>
                        </select>
                    </div>
                    
                    <div class="col-25" style="margin-left: 20">
                        <label for="country">Discipline</label>
                    </div>
                    <div class="col-75" style="margin-left: 20">
                        <select id="discipline" name="discipline">
                            <?php
                                    foreach ($disciplinesArray as $discipline){
                                        echo "<option value=\"$discipline\">$discipline</option>";
                                    }                            
                            ?>
                        </select>
                    </div>
                    
                    <div class="col-25" style="margin-left: 20">
                        <label for="gender">Gender</label>
                    </div>
                    <div class="col-75" style="margin-left: 20">
                        <select id="gender" name="gender">
                            <option value="M">Male</option>
                            <option value="F">Female</option>
                        </select>
                    </div>
                </div>
                
                <div class="row">
                    <input class="btn btn-primary btn-lg"style="margin-left: 35%" type="submit" value="Submit" onmouseover="getScoresheetID()">
                </div>
            </form>
        </div>
            
        <script>
            function getScoresheetID(){
                var discipline = $("#discipline").find(":selected").val();
                var ageCategory = $("#ageCategory").find(":selected").val();
                var gender = $("#gender").find(":selected").val();
                var tournamentID = $("#tournamentID").text();
                
//                alert(discipline + ageCategory + gender + tournamentID);
                
                $.post("./getScoresheetByAgeCategoryGender.php",
                {discipline: discipline,
                 ageCategory: ageCategory,
                 gender: gender,
                 tournamentID: tournamentID},
                function(data){
                   $("#scoresheetID").val(data); 
                });
            }
        </script>
            
    </body>
</html>