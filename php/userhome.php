<?php
session_start();
include './connect.php';
include './bootstrap.php';
$username = $_SESSION["username"];
$clubName = getClubName($username);

function getClubName($un): string
{
    include './connect.php';
    $clubName = "";
    $sql = "SELECT *
            FROM account
            WHERE Username = '$un';";
    $result = mysqli_query($con, $sql);
        
    if (mysqli_num_rows($result) > 0)
    {
        while($row = $result->fetch_assoc()) 
        {
            $clubName = $row["ClubName"];
            return $clubName;
        }
    }
}
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Welcome!</title>
        <link href="style.css"  rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <h1 align="center"> Hello <?php echo $clubName; ?></h1>
        
        <form action='../php/user/myAthletes.php' request='get'>
            <input type="submit" value="My Athletes">
        </form>         
        <form action='../php/user/showActiveTournaments.php' request='get'>
            <input type="submit" value="Register for tournament">
        </form>
    </body>
</html>

