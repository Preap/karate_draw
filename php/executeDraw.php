<?php
include 'connect.php';
include 'bootstrap.php';
    
include 'fillStacks.php';
?>
    
    
<!DOCTYPE html>
    
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        
    <?php
    include 'connect.php';
    include 'byeQueues.php';
        
        
        
    $positions = array();
        
//    echo ("numberOfAthletes: " . $numberOfAthletes."<br>");
    
    $scoreSheetSize = getScoresheetSize($numberOfAthletes);
        
//    echo ("scoreSheetSize: " . $scoreSheetSize."<br>");
    
    $byePositions = $q{$numberOfAthletes};
//    echo ($q{$numberOfAthletes}[0]);
//    echo $byePositions[0]." before for loop <br>";
    
    for($i=1;$i <= $scoreSheetSize;$i++) //put players in positions array
    {
//        echo $byePositions[0]." in for loop $i <br>";
        $bigP = findBiggestStack($stack);
        if(otherStacksEmpty($stack,$numberOfTeams) || $numberOfTeams==1)
        {
                $control = $byePositions[0];
                if($i == $control )
                {
                    $positions[$i] = null;
//                    echo "position $i is BYE <br>";
                    $q{$numberOfAthletes}->dequeue();
                }
                else
                {
                    $positions[$i] = $stack[$bigP]->pop();
//                    echo "position $i is not BYE <br>";
                }
        }
//        else if($byeSpace)
//        {
//            $opponent = findRandomStack($stack, $numberOfTeams);
//            $positions[$i] = $stack[$bigP]->pop();
//            $i++;
//            if($i % $byeSpace == 0)
//            {
//                $positions[$i] = null;
//            }
//            else
//            {
//                $positions[$i] = $stack[$opponent]->pop();
//            }
//        }
        else
        {
            $opponent = findRandomStack($stack, $numberOfTeams);
            $positions[$i] = $stack[$bigP]->pop();
//            echo "position $i is NOT BYE <br>";
            $i++;
            $control = $byePositions[0];
            if($i == $control )
            {
                $positions[$i] = null;
//                echo "position $i is BYE <br>";
                $q{$numberOfAthletes}->dequeue();
            }
            else
            {
                $positions[$i] = $stack[$opponent]->pop();
//                echo "position $i is NOT BYE <br>";
            }
        }
    }
        
    for($i=1;$i <= $scoreSheetSize;$i++) //print draw
    {        
        $athleteID=$positions[$i];
            
        $sql = "SELECT athlete.clubName
                FROM athlete 
                WHERE athlete.athleteID = '$athleteID';";
                    
        $result = $con->query($sql);
            
        if ($result->num_rows > 0) 
        {
            while($row = $result->fetch_assoc())  
            {
//                echo $row["clubName"]." ".$positions[$i];
                if($i%2==0)
                {
//                    echo "<br>";
                }
                else
                {
//                    echo "--------VS--------";
                }
            }
        }
    }
    $names = [];
    for($i = 1; $i <= $scoreSheetSize;$i++)
    {
        $athID = $positions[$i];
        if($athID)
        {
            $names[$i] = getPlayersNames($scoresheetID,$athID);
        }
        else
        {
            $names[$i] = null;
        }
    }
//    print_r($names);
    
    $_SESSION["names"]=$names;
    createScoresheetString($scoresheetID, $names);
        
        
    function findBiggestStack($stack)
    {
        $max = -9;
        $index=0;
            
        for($i = 0; $i < count($stack);$i++)
        {
            $size = $stack[$i]->count();
            if($size > $max)
            {
                $max= $size;
                $index = $i;
            }
        }
            
        return $index;
    }
        
        
        
    function findRandomStack($stack,$numOfTeams)
    {
        $big = findBiggestStack($stack);
        do 
        {
            $random = rand(0, $numOfTeams -1);
        } while ($random == $big || ($stack[$random]->isEmpty()));
            
        return $random;
    }
        
    function otherStacksEmpty ($stack,$numOfTeams)
    {
        $empty = 0;
            
        for($i = 0; $i < $numOfTeams ; $i++)
        {
            if($stack[$i]->isEmpty())
            {
                $empty++;
            }
        }
            
        if($empty == $numOfTeams-1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
        
    //make a array with player names to use in the presentation of the draw
    function getPlayersNames($scoresheetID,$athleteID) //IDs are stored in positions array
    {
        include 'connect.php';
        $name = "x";
        $sql = "SELECT athlete.firstName,athlete.lastName
                FROM athlete
                NATURAL JOIN participants
                WHERE participants.athleteID = '$athleteID' AND participants.scoresheetID = '$scoresheetID';";
                    
        $result = $con->query($sql);
            
        if ($result->num_rows > 0) 
        {
            while($row = $result->fetch_assoc())  
            {
                $name = $row["firstName"]." ".$row["lastName"];                
            }//end while
         }//end if
         return $name;
    }//end function
        
    //create initial scoresheet string
    function createScoresheetString($scoresheetID,$names)
    {
        include 'connect.php';
            
//        echo ("<br>count Names: ".count($names)."<br>");
    
        $scoresheetSize = getScoresheetSize(count($names));
            
        $sql = "SELECT `data` 
        FROM `initialscoresheet` 
        WHERE `num`=$scoresheetSize;";
            
        $result = mysqli_query($con, $sql);
        if (mysqli_num_rows($result) > 0)
        {
            while($row = $result->fetch_assoc()) 
            {
                $data = $row["data"];
               $myJson = json_decode($data);
               $j = 1;
               for($i=0;$i<count($myJson->teams);$i++)      //insert names in the new scoresheet
               {
                   $myJson->teams[$i][0]=$names[$j];
                   $j++;
                   $myJson->teams[$i][1]=$names[$j];
                   $j++;
               }
                $myJson = json_encode($myJson);
                $sql = "UPDATE `scoresheet` SET `data` = '$myJson' 
                      WHERE `scoresheet`.`scoresheetID` = '$scoresheetID';";  //to change 1 with scoresheetID
                          
                if ($con->query($sql) === TRUE) 
                {
//                    echo "Score updated";
                } 
                else 
                {
                    echo "Error: " . $sql . "<br>" . $con->error;
                }
            }
        }
        else
        {
            echo "No scoresheet found";
        }
    }
        
    //THIS FUNCTION RETURNS THE SIZE OF THE SCORESHEET 
    //DEPENDING ON HOW MANY ATHLETES ARE REGISTERED
    function getScoresheetSize($numberOfAthletes)
    {
        if($numberOfAthletes < 3)
        {
            $scoresheetSize = 2;
        }
        else if($numberOfAthletes <5)
        {
            $scoresheetSize = 4;
        }
        else if($numberOfAthletes < 9)
        {
            $scoresheetSize = 8;
        }
        else if($numberOfAthletes < 17)
        {
            $scoresheetSize = 16;
        }
        else if($numberOfAthletes < 33)
        {
            $scoresheetSize = 32;
        }
        else
        {
            $scoresheetSize = 64;
        }
            
        return $scoresheetSize;
    }
                ?>
        <h1 align="center" style="margin-top: 100px;font-size: 60px" >Are you sure you want to execute the draw?</h1>
        <div style="position: relative; top:50%" align="center">
            <a href="../jquery-bracket-master/preview.php">
                <button id="singlebutton" name="singlebutton" class="btn btn-lg btn-success center-block">YES!</button>
            </a>
            <a href="../php/showActiveTournaments.php">
                <button  id="singlebutton" name="singlebutton" class="btn btn-lg btn-secondary center-block">NO!</button>
            </a>
        </div>
    </body>
</html>