<?php

//create byeQueues for every possible draw
for($i=1;$i<=64;$i++)
{
    $q{$i} = new SplQueue();
}


//DRAW FOR 1
$q{1}->enqueue(0);

//DRAW FOR 2
$q{2}->enqueue(0);

//DRAW FOR 3
$q{3}->enqueue(3);
$q{3}->enqueue(0);

//DRAW FOR 4
$q{4}->enqueue(0);

//DRAW FOR 5
$q{5}->enqueue(2);
$q{5}->enqueue(6);
$q{5}->enqueue(7);
$q{5}->enqueue(0);

//DRAW FOR 6
$q{6}->enqueue(4);
$q{6}->enqueue(7);
$q{6}->enqueue(0);

//DRAW FOR 7
$q{7}->enqueue(6);
$q{7}->enqueue(0);

//DRAW FOR 8
$q{8}->enqueue(0);

//DRAW FOR 9
$q{9}->enqueue(4);
$q{9}->enqueue(6);
$q{9}->enqueue(8);
$q{9}->enqueue(10);
$q{9}->enqueue(12);
$q{9}->enqueue(14);
$q{9}->enqueue(15);
$q{9}->enqueue(0);

//DRAW FOR 10
$q{10}->enqueue(4);
$q{10}->enqueue(6);
$q{10}->enqueue(8);
$q{10}->enqueue(10);
$q{10}->enqueue(12);
$q{10}->enqueue(14);
$q{10}->enqueue(0);

//DRAW FOR 11
$q{11}->enqueue(4);
$q{11}->enqueue(6);
$q{11}->enqueue(8);
$q{11}->enqueue(10);
$q{11}->enqueue(12);
$q{11}->enqueue(0);

//DRAW FOR 12
$q{12}->enqueue(2);
$q{12}->enqueue(6);
$q{12}->enqueue(10);
$q{12}->enqueue(14);
$q{12}->enqueue(0);

//DRAW FOR 13
$q{13}->enqueue(4);
$q{13}->enqueue(10);
$q{13}->enqueue(15);
$q{13}->enqueue(0);

//DRAW FOR 14
$q{14}->enqueue(6);
$q{14}->enqueue(12);
$q{14}->enqueue(0);

//DRAW FOR 15
$q{15}->enqueue(15);
$q{15}->enqueue(0);

//DRAW FOR 16
$q{16}->enqueue(0);

//DRAW FOR 17
$q{17}->enqueue(2);
$q{17}->enqueue(6);
$q{17}->enqueue(8);
$q{17}->enqueue(10);
$q{17}->enqueue(12);
$q{17}->enqueue(14);
$q{17}->enqueue(16);
$q{17}->enqueue(18);
$q{17}->enqueue(20);
$q{17}->enqueue(22);
$q{17}->enqueue(24);
$q{17}->enqueue(26);
$q{17}->enqueue(28);
$q{17}->enqueue(30);
$q{17}->enqueue(31);
$q{17}->enqueue(0);

//DRAW FOR 18
$q{18}->enqueue(2);
$q{18}->enqueue(6);
$q{18}->enqueue(8);
$q{18}->enqueue(10);
$q{18}->enqueue(12);
$q{18}->enqueue(14);
$q{18}->enqueue(16);
$q{18}->enqueue(18);
$q{18}->enqueue(20);
$q{18}->enqueue(22);
$q{18}->enqueue(24);
$q{18}->enqueue(26);
$q{18}->enqueue(28);
$q{18}->enqueue(31);
$q{18}->enqueue(0);


//DRAW FOR 19
$q{19}->enqueue(2);
$q{19}->enqueue(6);
$q{19}->enqueue(8);
$q{19}->enqueue(10);
$q{19}->enqueue(12);
$q{19}->enqueue(14);
$q{19}->enqueue(18);
$q{19}->enqueue(20);
$q{19}->enqueue(22);
$q{19}->enqueue(24);
$q{19}->enqueue(26);
$q{19}->enqueue(28);
$q{19}->enqueue(31);
$q{19}->enqueue(0);

//DRAW FOR 20
$q{20}->enqueue(2);
$q{20}->enqueue(6);
$q{20}->enqueue(8);
$q{20}->enqueue(10);
$q{20}->enqueue(12);
$q{20}->enqueue(14);
$q{20}->enqueue(20);
$q{20}->enqueue(22);
$q{20}->enqueue(24);
$q{20}->enqueue(26);
$q{20}->enqueue(28);
$q{20}->enqueue(31);
$q{20}->enqueue(0);

//DRAW FOR 21
$q{21}->enqueue(2);
$q{21}->enqueue(6);
$q{21}->enqueue(10);
$q{21}->enqueue(12);
$q{21}->enqueue(14);
$q{21}->enqueue(20);
$q{21}->enqueue(22);
$q{21}->enqueue(24);
$q{21}->enqueue(26);
$q{21}->enqueue(28);
$q{21}->enqueue(31);
$q{21}->enqueue(0);

//DRAW FOR 22
$q{22}->enqueue(2);
$q{22}->enqueue(6);
$q{22}->enqueue(10);
$q{22}->enqueue(12);
$q{22}->enqueue(14);
$q{22}->enqueue(20);
$q{22}->enqueue(22);
$q{22}->enqueue(24);
$q{22}->enqueue(28);
$q{22}->enqueue(31);
$q{22}->enqueue(0);


//DRAW FOR 23
$q{23}->enqueue(2);
$q{23}->enqueue(6);
$q{23}->enqueue(10);
$q{23}->enqueue(14);
$q{23}->enqueue(20);
$q{23}->enqueue(22);
$q{23}->enqueue(24);
$q{23}->enqueue(28);
$q{23}->enqueue(31);
$q{23}->enqueue(0);

//DRAW FOR 24
$q{24}->enqueue(2);
$q{24}->enqueue(6);
$q{24}->enqueue(10);
$q{24}->enqueue(14);
$q{24}->enqueue(20);
$q{24}->enqueue(24);
$q{24}->enqueue(28);
$q{24}->enqueue(31);
$q{24}->enqueue(0);

//DRAW FOR 25
$q{25}->enqueue(2);
$q{25}->enqueue(8);
$q{25}->enqueue(14);
$q{25}->enqueue(20);
$q{25}->enqueue(24);
$q{25}->enqueue(26);
$q{25}->enqueue(31);
$q{25}->enqueue(0);

//DRAW FOR 26
$q{26}->enqueue(2);
$q{26}->enqueue(8);
$q{26}->enqueue(12);
$q{26}->enqueue(20);
$q{26}->enqueue(26);
$q{26}->enqueue(31);
$q{26}->enqueue(0);

//DRAW FOR 27
$q{27}->enqueue(4);
$q{27}->enqueue(10);
$q{27}->enqueue(20);
$q{27}->enqueue(26);
$q{27}->enqueue(31);
$q{27}->enqueue(0);

//DRAW FOR 28
$q{28}->enqueue(4);
$q{28}->enqueue(10);
$q{28}->enqueue(22);
$q{28}->enqueue(28);
$q{28}->enqueue(0);

//DRAW FOR 29
$q{29}->enqueue(8);
$q{29}->enqueue(16);
$q{29}->enqueue(24);
$q{29}->enqueue(0);

//DRAW FOR 30
$q{30}->enqueue(8);
$q{30}->enqueue(24);
$q{30}->enqueue(0);

//DRAW FOR 31
$q{31}->enqueue(31);
$q{31}->enqueue(0);

//DRAW FOR 32
$q{32}->enqueue(0);

//DRAW FOR 33
$q{33}->enqueue(2);
$q{33}->enqueue(4);
$q{33}->enqueue(6);
$q{33}->enqueue(8);
$q{33}->enqueue(10);
$q{33}->enqueue(12);
$q{33}->enqueue(14);
$q{33}->enqueue(16);
$q{33}->enqueue(18);
$q{33}->enqueue(20);
$q{33}->enqueue(22);
$q{33}->enqueue(24);
$q{33}->enqueue(26);
$q{33}->enqueue(30);
$q{33}->enqueue(32);
$q{33}->enqueue(34);
$q{33}->enqueue(36);
$q{33}->enqueue(38);
$q{33}->enqueue(40);
$q{33}->enqueue(42);
$q{33}->enqueue(44);
$q{33}->enqueue(46);
$q{33}->enqueue(48);
$q{33}->enqueue(50);
$q{33}->enqueue(52);
$q{33}->enqueue(54);
$q{33}->enqueue(56);
$q{33}->enqueue(58);
$q{33}->enqueue(60);
$q{33}->enqueue(62);
$q{33}->enqueue(63);
$q{33}->enqueue(0);

//DRAW FOR 34
$q{34}->enqueue(2);
$q{34}->enqueue(4);
$q{34}->enqueue(6);
$q{34}->enqueue(8);
$q{34}->enqueue(10);
$q{34}->enqueue(12);
$q{34}->enqueue(14);
$q{34}->enqueue(16);
$q{34}->enqueue(18);
$q{34}->enqueue(20);
$q{34}->enqueue(22);
$q{34}->enqueue(24);
$q{34}->enqueue(26);
$q{34}->enqueue(30);
$q{34}->enqueue(32);
$q{34}->enqueue(34);
$q{34}->enqueue(36);
$q{34}->enqueue(40);
$q{34}->enqueue(42);
$q{34}->enqueue(44);
$q{34}->enqueue(46);
$q{34}->enqueue(48);
$q{34}->enqueue(50);
$q{34}->enqueue(52);
$q{34}->enqueue(54);
$q{34}->enqueue(56);
$q{34}->enqueue(58);
$q{34}->enqueue(60);
$q{34}->enqueue(62);
$q{34}->enqueue(63);
$q{34}->enqueue(0);

//DRAW FOR 35
$q{35}->enqueue(2);
$q{35}->enqueue(4);
$q{35}->enqueue(8);
$q{35}->enqueue(10);
$q{35}->enqueue(12);
$q{35}->enqueue(14);
$q{35}->enqueue(16);
$q{35}->enqueue(18);
$q{35}->enqueue(20);
$q{35}->enqueue(22);
$q{35}->enqueue(24);
$q{35}->enqueue(26);
$q{35}->enqueue(30);
$q{35}->enqueue(32);
$q{35}->enqueue(34);
$q{35}->enqueue(36);
$q{35}->enqueue(40);
$q{35}->enqueue(42);
$q{35}->enqueue(44);
$q{35}->enqueue(46);
$q{35}->enqueue(48);
$q{35}->enqueue(50);
$q{35}->enqueue(52);
$q{35}->enqueue(54);
$q{35}->enqueue(56);
$q{35}->enqueue(58);
$q{35}->enqueue(60);
$q{35}->enqueue(62);
$q{35}->enqueue(63);
$q{35}->enqueue(0);

//DRAW FOR 36
$q{36}->enqueue(2);
$q{36}->enqueue(4);
$q{36}->enqueue(8);
$q{36}->enqueue(10);
$q{36}->enqueue(12);
$q{36}->enqueue(14);
$q{36}->enqueue(16);
$q{36}->enqueue(18);
$q{36}->enqueue(20);
$q{36}->enqueue(22);
$q{36}->enqueue(24);
$q{36}->enqueue(26);
$q{36}->enqueue(30);
$q{36}->enqueue(32);
$q{36}->enqueue(34);
$q{36}->enqueue(36);
$q{36}->enqueue(40);
$q{36}->enqueue(42);
$q{36}->enqueue(44);
$q{36}->enqueue(46);
$q{36}->enqueue(48);
$q{36}->enqueue(50);
$q{36}->enqueue(52);
$q{36}->enqueue(56);
$q{36}->enqueue(58);
$q{36}->enqueue(60);
$q{36}->enqueue(62);
$q{36}->enqueue(63);
$q{36}->enqueue(0);

//DRAW FOR 37
$q{37}->enqueue(2);
$q{37}->enqueue(4);
$q{37}->enqueue(8);
$q{37}->enqueue(10);
$q{37}->enqueue(12);
$q{37}->enqueue(14);
$q{37}->enqueue(16);
$q{37}->enqueue(18);
$q{37}->enqueue(20);
$q{37}->enqueue(22);
$q{37}->enqueue(26);
$q{37}->enqueue(30);
$q{37}->enqueue(32);
$q{37}->enqueue(34);
$q{37}->enqueue(36);
$q{37}->enqueue(40);
$q{37}->enqueue(42);
$q{37}->enqueue(44);
$q{37}->enqueue(46);
$q{37}->enqueue(48);
$q{37}->enqueue(50);
$q{37}->enqueue(52);
$q{37}->enqueue(56);
$q{37}->enqueue(58);
$q{37}->enqueue(60);
$q{37}->enqueue(62);
$q{37}->enqueue(63);
$q{37}->enqueue(0);

//DRAW FOR 38
$q{38}->enqueue(2);
$q{38}->enqueue(4);
$q{38}->enqueue(8);
$q{38}->enqueue(10);
$q{38}->enqueue(12);
$q{38}->enqueue(14);
$q{38}->enqueue(16);
$q{38}->enqueue(18);
$q{38}->enqueue(20);
$q{38}->enqueue(22);
$q{38}->enqueue(26);
$q{38}->enqueue(30);
$q{38}->enqueue(32);
$q{38}->enqueue(34);
$q{38}->enqueue(36);
$q{38}->enqueue(40);
$q{38}->enqueue(44);
$q{38}->enqueue(46);
$q{38}->enqueue(48);
$q{38}->enqueue(50);
$q{38}->enqueue(52);
$q{38}->enqueue(56);
$q{38}->enqueue(58);
$q{38}->enqueue(60);
$q{38}->enqueue(62);
$q{38}->enqueue(63);
$q{38}->enqueue(0);

//DRAW FOR 39
$q{39}->enqueue(2);
$q{39}->enqueue(4);
$q{39}->enqueue(8);
$q{39}->enqueue(10);
$q{39}->enqueue(12);
$q{39}->enqueue(16);
$q{39}->enqueue(18);
$q{39}->enqueue(20);
$q{39}->enqueue(22);
$q{39}->enqueue(26);
$q{39}->enqueue(30);
$q{39}->enqueue(32);
$q{39}->enqueue(34);
$q{39}->enqueue(36);
$q{39}->enqueue(40);
$q{39}->enqueue(44);
$q{39}->enqueue(46);
$q{39}->enqueue(48);
$q{39}->enqueue(50);
$q{39}->enqueue(52);
$q{39}->enqueue(56);
$q{39}->enqueue(58);
$q{39}->enqueue(60);
$q{39}->enqueue(62);
$q{39}->enqueue(63);
$q{39}->enqueue(0);

//DRAW FOR 40
$q{40}->enqueue(2);
$q{40}->enqueue(4);
$q{40}->enqueue(8);
$q{40}->enqueue(10);
$q{40}->enqueue(12);
$q{40}->enqueue(16);
$q{40}->enqueue(18);
$q{40}->enqueue(20);
$q{40}->enqueue(22);
$q{40}->enqueue(26);
$q{40}->enqueue(30);
$q{40}->enqueue(32);
$q{40}->enqueue(34);
$q{40}->enqueue(36);
$q{40}->enqueue(40);
$q{40}->enqueue(44);
$q{40}->enqueue(46);
$q{40}->enqueue(48);
$q{40}->enqueue(50);
$q{40}->enqueue(52);
$q{40}->enqueue(56);
$q{40}->enqueue(60);
$q{40}->enqueue(62);
$q{40}->enqueue(63);
$q{40}->enqueue(0);

//DRAW FOR 41
$q{41}->enqueue(2);
$q{41}->enqueue(4);
$q{41}->enqueue(8);
$q{41}->enqueue(10);
$q{41}->enqueue(12);
$q{41}->enqueue(16);
$q{41}->enqueue(18);
$q{41}->enqueue(22);
$q{41}->enqueue(26);
$q{41}->enqueue(30);
$q{41}->enqueue(32);
$q{41}->enqueue(34);
$q{41}->enqueue(36);
$q{41}->enqueue(40);
$q{41}->enqueue(44);
$q{41}->enqueue(46);
$q{41}->enqueue(48);
$q{41}->enqueue(50);
$q{41}->enqueue(52);
$q{41}->enqueue(56);
$q{41}->enqueue(60);
$q{41}->enqueue(62);
$q{41}->enqueue(63);
$q{41}->enqueue(0);

//DRAW FOR 42
$q{42}->enqueue(2);
$q{42}->enqueue(4);
$q{42}->enqueue(8);
$q{42}->enqueue(10);
$q{42}->enqueue(12);
$q{42}->enqueue(16);
$q{42}->enqueue(18);
$q{42}->enqueue(22);
$q{42}->enqueue(26);
$q{42}->enqueue(30);
$q{42}->enqueue(32);
$q{42}->enqueue(34);
$q{42}->enqueue(36);
$q{42}->enqueue(40);
$q{42}->enqueue(44);
$q{42}->enqueue(48);
$q{42}->enqueue(50);
$q{42}->enqueue(52);
$q{42}->enqueue(56);
$q{42}->enqueue(60);
$q{42}->enqueue(62);
$q{42}->enqueue(63);
$q{42}->enqueue(0);

//DRAW FOR 43
$q{43}->enqueue(2);
$q{43}->enqueue(4);
$q{43}->enqueue(8);
$q{43}->enqueue(12);
$q{43}->enqueue(16);
$q{43}->enqueue(18);
$q{43}->enqueue(22);
$q{43}->enqueue(26);
$q{43}->enqueue(30);
$q{43}->enqueue(32);
$q{43}->enqueue(34);
$q{43}->enqueue(36);
$q{43}->enqueue(40);
$q{43}->enqueue(44);
$q{43}->enqueue(48);
$q{43}->enqueue(50);
$q{43}->enqueue(52);
$q{43}->enqueue(56);
$q{43}->enqueue(60);
$q{43}->enqueue(62);
$q{43}->enqueue(63);
$q{43}->enqueue(0);

//DRAW FOR 44
$q{44}->enqueue(2);
$q{44}->enqueue(4);
$q{44}->enqueue(8);
$q{44}->enqueue(12);
$q{44}->enqueue(16);
$q{44}->enqueue(18);
$q{44}->enqueue(22);
$q{44}->enqueue(26);
$q{44}->enqueue(30);
$q{44}->enqueue(32);
$q{44}->enqueue(34);
$q{44}->enqueue(36);
$q{44}->enqueue(40);
$q{44}->enqueue(44);
$q{44}->enqueue(48);
$q{44}->enqueue(52);
$q{44}->enqueue(56);
$q{44}->enqueue(60);
$q{44}->enqueue(62);
$q{44}->enqueue(63);
$q{44}->enqueue(0);


//DRAW FOR 45
$q{45}->enqueue(2);
$q{45}->enqueue(4);
$q{45}->enqueue(8);
$q{45}->enqueue(12);
$q{45}->enqueue(16);
$q{45}->enqueue(18);
$q{45}->enqueue(22);
$q{45}->enqueue(26);
$q{45}->enqueue(30);
$q{45}->enqueue(34);
$q{45}->enqueue(36);
$q{45}->enqueue(40);
$q{45}->enqueue(44);
$q{45}->enqueue(48);
$q{45}->enqueue(52);
$q{45}->enqueue(56);
$q{45}->enqueue(60);
$q{45}->enqueue(62);
$q{45}->enqueue(63);
$q{45}->enqueue(0);

//DRAW FOR 46
$q{46}->enqueue(2);
$q{46}->enqueue(4);
$q{46}->enqueue(8);
$q{46}->enqueue(12);
$q{46}->enqueue(16);
$q{46}->enqueue(18);
$q{46}->enqueue(22);
$q{46}->enqueue(26);
$q{46}->enqueue(30);
$q{46}->enqueue(34);
$q{46}->enqueue(40);
$q{46}->enqueue(44);
$q{46}->enqueue(48);
$q{46}->enqueue(52);
$q{46}->enqueue(56);
$q{46}->enqueue(60);
$q{46}->enqueue(62);
$q{46}->enqueue(63);
$q{46}->enqueue(0);

//DRAW FOR 47
$q{47}->enqueue(4);
$q{47}->enqueue(8);
$q{47}->enqueue(12);
$q{47}->enqueue(16);
$q{47}->enqueue(18);
$q{47}->enqueue(22);
$q{47}->enqueue(26);
$q{47}->enqueue(30);
$q{47}->enqueue(34);
$q{47}->enqueue(40);
$q{47}->enqueue(44);
$q{47}->enqueue(48);
$q{47}->enqueue(52);
$q{47}->enqueue(56);
$q{47}->enqueue(60);
$q{47}->enqueue(62);
$q{47}->enqueue(63);
$q{47}->enqueue(0);

//DRAW FOR 48
$q{48}->enqueue(4);
$q{48}->enqueue(8);
$q{48}->enqueue(12);
$q{48}->enqueue(16);
$q{48}->enqueue(18);
$q{48}->enqueue(22);
$q{48}->enqueue(26);
$q{48}->enqueue(30);
$q{48}->enqueue(34);
$q{48}->enqueue(40);
$q{48}->enqueue(44);
$q{48}->enqueue(48);
$q{48}->enqueue(52);
$q{48}->enqueue(56);
$q{48}->enqueue(60);
$q{48}->enqueue(63);
$q{48}->enqueue(0);

//DRAW FOR 49
$q{49}->enqueue(4);
$q{49}->enqueue(8);
$q{49}->enqueue(12);
$q{49}->enqueue(16);
$q{49}->enqueue(22);
$q{49}->enqueue(26);
$q{49}->enqueue(30);
$q{49}->enqueue(34);
$q{49}->enqueue(40);
$q{49}->enqueue(44);
$q{49}->enqueue(48);
$q{49}->enqueue(52);
$q{49}->enqueue(56);
$q{49}->enqueue(60);
$q{49}->enqueue(63);
$q{49}->enqueue(0);

//DRAW FOR 50
$q{50}->enqueue(4);
$q{50}->enqueue(8);
$q{50}->enqueue(12);
$q{50}->enqueue(16);
$q{50}->enqueue(22);
$q{50}->enqueue(26);
$q{50}->enqueue(30);
$q{50}->enqueue(34);
$q{50}->enqueue(40);
$q{50}->enqueue(44);
$q{50}->enqueue(48);
$q{50}->enqueue(52);
$q{50}->enqueue(56);
$q{50}->enqueue(63);
$q{50}->enqueue(0);

//DRAW FOR 51
$q{51}->enqueue(4);
$q{51}->enqueue(12);
$q{51}->enqueue(16);
$q{51}->enqueue(22);
$q{51}->enqueue(26);
$q{51}->enqueue(30);
$q{51}->enqueue(34);
$q{51}->enqueue(40);
$q{51}->enqueue(44);
$q{51}->enqueue(48);
$q{51}->enqueue(52);
$q{51}->enqueue(56);
$q{51}->enqueue(63);
$q{51}->enqueue(0);

//DRAW FOR 52
$q{52}->enqueue(4);
$q{52}->enqueue(12);
$q{52}->enqueue(16);
$q{52}->enqueue(22);
$q{52}->enqueue(26);
$q{52}->enqueue(30);
$q{52}->enqueue(34);
$q{52}->enqueue(40);
$q{52}->enqueue(48);
$q{52}->enqueue(52);
$q{52}->enqueue(56);
$q{52}->enqueue(63);
$q{52}->enqueue(0);

//DRAW FOR 53
$q{53}->enqueue(4);
$q{53}->enqueue(12);
$q{53}->enqueue(22);
$q{53}->enqueue(26);
$q{53}->enqueue(30);
$q{53}->enqueue(34);
$q{53}->enqueue(40);
$q{53}->enqueue(48);
$q{53}->enqueue(52);
$q{53}->enqueue(56);
$q{53}->enqueue(63);
$q{53}->enqueue(0);

//DRAW FOR 54
$q{54}->enqueue(4);
$q{54}->enqueue(12);
$q{54}->enqueue(22);
$q{54}->enqueue(26);
$q{54}->enqueue(30);
$q{54}->enqueue(34);
$q{54}->enqueue(40);
$q{54}->enqueue(48);
$q{54}->enqueue(56);
$q{54}->enqueue(63);
$q{54}->enqueue(0);

//DRAW FOR 55
$q{55}->enqueue(4);
$q{55}->enqueue(12);
$q{55}->enqueue(22);
$q{55}->enqueue(30);
$q{55}->enqueue(34);
$q{55}->enqueue(40);
$q{55}->enqueue(48);
$q{55}->enqueue(56);
$q{55}->enqueue(63);
$q{55}->enqueue(0);

//DRAW FOR 56
$q{56}->enqueue(4);
$q{56}->enqueue(12);
$q{56}->enqueue(22);
$q{56}->enqueue(30);
$q{56}->enqueue(38);
$q{56}->enqueue(48);
$q{56}->enqueue(56);
$q{56}->enqueue(63);
$q{56}->enqueue(0);

//DRAW FOR 57
$q{57}->enqueue(6);
$q{57}->enqueue(22);
$q{57}->enqueue(30);
$q{57}->enqueue(40);
$q{57}->enqueue(48);
$q{57}->enqueue(56);
$q{57}->enqueue(63);
$q{57}->enqueue(0);


//DRAW FOR 58
$q{58}->enqueue(6);
$q{58}->enqueue(16);
$q{58}->enqueue(30);
$q{58}->enqueue(38);
$q{58}->enqueue(46);
$q{58}->enqueue(54);
$q{58}->enqueue(0);

//DRAW FOR 59
$q{59}->enqueue(6);
$q{59}->enqueue(16);
$q{59}->enqueue(38);
$q{59}->enqueue(46);
$q{59}->enqueue(54);
$q{59}->enqueue(0);

//DRAW FOR 60
$q{60}->enqueue(6);
$q{60}->enqueue(22);
$q{60}->enqueue(38);
$q{60}->enqueue(54);
$q{60}->enqueue(0);

//DRAW FOR 61
$q{61}->enqueue(22);
$q{61}->enqueue(38);
$q{61}->enqueue(54);
$q{61}->enqueue(0);

//DRAW FOR 62
$q{62}->enqueue(14);
$q{62}->enqueue(50);
$q{62}->enqueue(0);

//DRAW FOR 63
$q{63}->enqueue(63);
$q{63}->enqueue(0);

//DRAW FOR 64
$q{64}->enqueue(0);