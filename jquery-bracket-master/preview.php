<?php
session_start();
$names = $_SESSION["names"];
?>

<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Testing jQuery Gracket Version 1.5.5</title>
        <link href="./dist//jquery.bracket.min.css" rel="stylesheet">
    </head>
    <body>
        <div id="scoresheetID" hidden="true"><?php echo $_SESSION["scoresheetID"] ?></div>
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.11.3.js"></script>
        <script src="./dist/jquery.bracket.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script> 
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <div align="center">
            <input class="btn btn-primary btn-lg" type="button" value="save" name="save" id="save"/>
        </div>
        <div class="demo" align="center" id="demo"></div>
        
<!--        <input type="button" value="getData" name="getData" id="getData"/>
        <input type="button" value="test" name="test" id="test"/>-->
        <!--Button trigger modal--> 
<!--        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
            Launch demo modal
        </button>-->
        
        <!-- Modal -->
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        ...
                        <input type="text" name="firstname" value="Mickey">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary" id="saveModal">OK</button>
                    </div>
                </div>
            </div>
        </div>
        <script> 
            var singleElimination;
            $("#save").bind("click",saveScoresheet);
                
                
            function getScoresheet()
            {
                scoreID = $("#scoresheetID").text();
                $.post("../php/getScoresheet.php",
                {
                    scoreID : scoreID
                },function(data)
                {
                    console.log("data returned not parsed: " +data);
                    json = JSON.parse(data);
                    //                    console.log(json);
                    singleElimination = json;
                    $.each(json.results[0],function(round,roundNum){
                        $.each($(this),function(match,matchNum){
                            json.results[0][round][match][0] = parseInt($(this)[0]);
                            json.results[0][round][match][1] = parseInt($(this)[1]);;
                        });
                    });
                    
                    {$('.demo').bracket({
                            init: singleElimination, // data to initialize
                            skipConsolationRound: true,
                            teamWidth: 200, // number
                            centerConnectors: true
                        });
                    }
                    bindings("called from getScoresheet()");
                    singleElimination=json;
                },"json");
            }
            getScoresheet();
//            $("#save").bind("click",saveScoresheet);
//            $("#getData").bind("click",getScoresheet);
//            $(".teamContainer").click(function(){
//                var firstPlayer = $(this).children(".team:first").children(".label").text();
//                var lastPlayer = $(this).children(".team:last").children(".label").text();
//                var firstScore = $(this).children().children(".score:first").text();
//                var lastScore = $(this).children().children(".score:last").text();
//                //                console.log("score is: " + firstScore);
//                $(".modal-body").html(firstPlayer +"<input type=\"text\" name=\"firstScore\" value=\"0\" id=\"firstScore\" size=\"1\" style=\"text-align: right\"><br>"+ lastPlayer +"<input type=\"text\" name=\"lastScore\" value=\"0\" id=\"lastScore\" size=\"1\" style=\"text-align: right\"><br>");
//                $("#firstScore").val(firstScore);  
//                $("#lastScore").val(lastScore);
//                $("#exampleModalCenter").modal('toggle');
//            });
            $("#saveModal").bind("click",saveModal);
            $(".label").bind("click",findScoreIndex);
                
                
            var rounds;
            var match;
            var score;
            var round;
            var json;
            
            function findScoreIndex()
            {
                rounds = $(".round").size(); 
                match = $(".match").size();
                var index = $(".label").index(this);
                //                console.log("index ID: "+index);
                var players = Math.pow(2,rounds);
                var initPlayers = players;
                round=0;
                score=0;
                var positions = new Array();
                
                for(var i = 0; i < rounds; i++)
                {
                    positions[i]=players - 1;
                    players = players + (initPlayers / Math.pow(2,i+1));
                   
                }
                
                var r=-1;
                do {
                    r++;
                    round=r;
                }
                while (index > positions[r]);
                
                            
                if (index % 2 == 0)
                {
                    score = 0;
                }
                else
                {
                    score = 1;
                }
                
                var counter = -1;
                var num=positions[round-1];
                if(round == 0)
                    num=-1;
                do {
                    num=num+2;
                    counter ++;
                }
                while (index > num);
                match = counter;
                
               
                
                //                console.log("result: "+ singleElimination.results[0][round][match][score]);
            }
            
//            $("#test").on("click",function(){
//                bindings("called on test clicked");
//            });
            
            
            
            function saveModal()
            {
                singleElimination.results[0][round][match][0]=parseInt($("#firstScore").val());
                singleElimination.results[0][round][match][1]=parseInt($("#lastScore").val());
                $("#exampleModalCenter").modal('toggle');
                {$('.demo').bracket({
                        init: singleElimination, // data to initialize
                        skipConsolationRound: true,
                        teamWidth: 200, // number
                        centerConnectors: true
                    });
                }
//                $("#save").bind("click",saveScoresheet);
//                $("#getData").bind("click",getScoresheet);
//                $(".teamContainer").bind("click",openModal);
//                $("#saveModal").bind("click",saveModal);
//                $(".label").bind("click",findScoreIndex);
                bindings("bindings called from saveModal()");
            }
            
//            $(".teamContainer").bind("click",openModal);    
            
            function saveScoresheet()
            {
                console.log("called saveScoresheet");
                $.post("../php/saveScoresheet.php",
                {
                    score : singleElimination
                                
                },function(data)
                {
                    console.log(data);                    
                });
            }
            
            function buildBracket()
            {
                $('.demo').bracket({
                    init: singleElimination, // data to initialize
                    skipConsolationRound: true,
                    teamWidth: 200, // number
                    centerConnectors: true
                });
                
            }
            
            function openModal()
            {
                var firstPlayer = $(this).children(".team:first").children(".label").text();
                var lastPlayer = $(this).children(".team:last").children(".label").text();
                var firstScore = $(this).children().children(".score:first").text();
                var lastScore = $(this).children().children(".score:last").text();
                //                console.log("score is: " + firstScore);
                $(".modal-body").html(firstPlayer +"<input type=\"text\" name=\"firstScore\" value=\"0\" id=\"firstScore\" size=\"1\" style=\"text-align: right;position: absolute; left:300px;border-radius: 5px\"><br>"
                        + lastPlayer +"<input type=\"text\" name=\"lastScore\" value=\"0\" id=\"lastScore\" size=\"1\" style=\"text-align: right; position: absolute; left:300px;border-radius: 5px;margin-top:5px\"><br>");
                $("#firstScore").val(firstScore);  
                $("#lastScore").val(lastScore);
                $("#exampleModalCenter").modal('toggle');
                console.log("round: " + round);
                console.log("match: " + match);
                console.log("score: " + score);
            }
            
            function bindings(msg)
            {
//                $("#save").bind("click",saveScoresheet);
//                $("#getData").bind("click",getScoresheet);
                $(".teamContainer").bind("click",openModal);
//                $("#saveModal").bind("click",saveModal);
                $(".label").bind("click",findScoreIndex);
                console.log(msg);
            }
        </script>
    </body>
</html>

